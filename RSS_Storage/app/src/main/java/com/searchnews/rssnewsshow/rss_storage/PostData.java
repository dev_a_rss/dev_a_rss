package com.searchnews.rssnewsshow.rss_storage;

/**
 * Created by sdragoev on 20.01.2017.
 */

public class PostData
{
    public String postThumbUrl;
    public String postTitle;
    public String postDescription;
    public String postDate;
    public String postLink;
}
