package com.searchnews.rssnewsshow.rss_storage.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.searchnews.rssnewsshow.rss_storage.PostData;
import com.searchnews.rssnewsshow.rss_storage.PostItemAdapter;
import com.searchnews.rssnewsshow.rss_storage.R;
import com.searchnews.rssnewsshow.rss_storage.ShowSelectedMessageActivity;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.support.design.R.id.time;
import static java.lang.Thread.sleep;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentEchoMSK.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentEchoMSK#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentEchoMSK extends Fragment
{
    final int MAX_SIZE_POST_DATA = 100;

    private PostData[]  listData;
    private PostData[]  listData_new;
    ListView            listView;
    int     number_elt = 0;

    Context     f_context;
    PostItemAdapter itemAdapter;

    HTTPDownloadTask    rss_packet;
    String              urlString;// = new String("http://echo.msk.ru/guests/9032/rss-fulltext.xml");

    int                 receive_bytes;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**************************** New code ******************************/
    private String title        = "title";
    private String link         = "link";
    private String description  = "description";
    private String pubDate      = "pubDate";
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean     parsingComplete = true;
    /**************************** New code ******************************/

    public FragmentEchoMSK()
    {
        // Required empty public constructor
    }
    //_________________________________________________________________________

    public void setStringURL(String  strURL)
    {
        // Required empty public constructor
        urlString = new String(strURL.toString());
    }
    //_________________________________________________________________________

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentImport.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentEchoMSK newInstance(String param1, String param2)
    {
        FragmentEchoMSK fragment = new FragmentEchoMSK();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    //_________________________________________________________________________

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        this.generateDummyData();
    }
    //_________________________________________________________________________

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        listView = (ListView) getActivity().findViewById(R.id.postListViewF);
        listView.setVisibility(View.INVISIBLE);

        itemAdapter.set_max_visible_element(MAX_SIZE_POST_DATA);
        listView.setAdapter(itemAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            private PostItemAdapter.ViewHolder viewHolder;

            @Override
            public void onItemClick(AdapterView adapterView, View itemClicked, int position, long id)
            {
                Intent  intentShowSiteWithMessage = new Intent(f_context, ShowSelectedMessageActivity.class);

                this.viewHolder = (PostItemAdapter.ViewHolder) itemClicked.getTag();

                this.viewHolder.postLinkView.setId(position);

                String  strURLMessage = new String(this.viewHolder.postLinkView.getText().toString());

                intentShowSiteWithMessage.putExtra("adress_site", strURLMessage.toString());

                startActivity(intentShowSiteWithMessage);
            }
        });
    }
    //_________________________________________________________________________

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        f_context = inflater.getContext();
        itemAdapter = new PostItemAdapter(inflater.getContext(), R.layout.postitem, listData);

        rss_packet = new HTTPDownloadTask();
        rss_packet.execute(urlString);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_echo_msk, container, false);
    }
    //_________________________________________________________________________

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }
    //_________________________________________________________________________

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }
    //_________________________________________________________________________

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    //_________________________________________________________________________

    private void generateDummyData()
    {
        PostData data = null;
        listData = new PostData[MAX_SIZE_POST_DATA];

        for (int i = 0; i < MAX_SIZE_POST_DATA; i++)
        {
            data = new PostData();

            data.postThumbUrl = null;
            data.postTitle = "Post " + (i + 1) + " Title: This is the Post Title from RSS Feed";
            data.postDescription = "This is description of item";
            data.postDate = "January 23, 2017";
            data.postLink = null;

            listData[i] = data;
        }
    }
    //_________________________________________________________________________

    private class DownloadRSSXML extends AsyncTask< String, String, String>
    {
        private String resp;
        //ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params)
        {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try
            {
                int time = Integer.parseInt(params[0])*1000;

                sleep(time);
                resp = "Slept for " + params[0] + " seconds";
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
                resp = e.getMessage();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                resp = e.getMessage();
            }

            return resp;
        }
        //_____________________________________________________________________

        @Override
        protected void onPostExecute(String result)
        {
            // execution of result of Long time consuming operation
            //progressDialog.dismiss();
            //finalResult.setText(result);
        }
        //_____________________________________________________________________

        @Override
        protected void onPreExecute()
        {
            //progressDialog = ProgressDialog.show(MainActivity.this,
            //        "ProgressDialog",
            //        "Wait for "+time.getText().toString()+ " seconds");
        }
        //_____________________________________________________________________

        @Override
        protected void onProgressUpdate(String... text)
        {
            //finalResult.setText(text[0]);
        }
        //_____________________________________________________________________
    }
    //_________________________________________________________________________

    /**************************************************************************
     *
     *                                   New code                          star
     *      * @return
     *************************************************************************/
    public String getTitle()
    {
        return title;
    }
    //_________________________________________________________________________

    public String getLink()
    {
        return link;
    }
    //_________________________________________________________________________

    public String getDescription()
    {
        return description;
    }
    //_________________________________________________________________________

    public String getDate()
    {
        return pubDate;
    }
    //_________________________________________________________________________

    public void parseXMLAndStoreIt(XmlPullParser myParser)
    {
        int event;
        String text=null;
        //int     number_elt = 0;
        int     counter_elements_item = 0;
        PostData    data = null;
        data = new PostData();
        number_elt = 0;
        int     x;

        //itemAdapter.clear();

        try {
            event = myParser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT)
            {
                String name = myParser.getName();

                switch (event)
                {
                    case XmlPullParser.START_TAG:
                        break;

                    case XmlPullParser.TEXT:
                                text = myParser.getText();
                                Log.d("text = ", text);

                        break;

                    case XmlPullParser.END_TAG:

                        if(name.equals("title")){
                            title = text;
                            data.postTitle = text;
                            Log.d("title = ", title);
                            counter_elements_item++;
                        }
                        else if(name.equals("link")){
                            link = text;
                            data.postLink = text;
                            Log.d("link = ", text);
                            counter_elements_item++;
                        }
                        else if(name.equals("description")){
                            description = text;
                            data.postDescription = text;
                            Log.d("description = ", title);
                            counter_elements_item++;
                        }
                        else if(name.equals("pubDate")
                                ||
                                name.equals("lastUpdated")
                            //    ||
                            //    name.equals("lastBuildDate")
                                )
                        {
                            pubDate = text;
                            data.postDate = text;
                            Log.d("Date = ", text);
                            counter_elements_item++;
                        }
                        else if (name.equals("item"))
                        {
                            listData[number_elt++] = data;

                            data = new PostData();
                        }
                        else if (name.equals("/item"))
                        {
                            x = 2;
                        }
                        else
                        {
                            x = 3;
                        }

                        break;
                }

                event = myParser.next();
            }

            parsingComplete = false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /***********************************************************************
     *
     *  ---------------------------  GET   REQUEST -------------------------
     *
     **********************************************************************/
    private class HTTPDownloadTask extends AsyncTask< String, Integer, PostData[]>
    {

        @Override
        protected PostData[] doInBackground(String... params)
        {
            // TODO Auto-generated method stub
            String urlStr = params[0];
            InputStream is = null;
            try
            {
                URL url = new URL(urlStr);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10*1000);
                connection.setConnectTimeout(10*1000);
                connection.setRequestMethod("GET");
                connection.setDoInput(true);

                connection.setChunkedStreamingMode(32000);    // Added 2017.01.22  10:52
                connection.setIfModifiedSince(0L);          // Added 2017.01.22  10:52

                connection.setRequestProperty( "charset", "utf-8");

                connection.connect();
                int response = connection.getResponseCode();
                Log.d("debug", "The response is: " + response);
                is = connection.getInputStream();

                xmlFactoryObject = XmlPullParserFactory.newInstance();
                XmlPullParser myparser = xmlFactoryObject.newPullParser();

                myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                myparser.setInput(is, null);

                parseXMLAndStoreIt(myparser);
                is.close();

                Log.d("debug  size counter = ", String.valueOf(receive_bytes));
            }
            catch (MalformedURLException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (XmlPullParserException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values)
        {
            super.onProgressUpdate(values);
        }
        //_________________________________________________

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        //_________________________________________________

        @Override
        protected void onPostExecute(PostData[] postDatas)
        {
            super.onPostExecute(postDatas);

            itemAdapter.set_max_visible_element(number_elt);

            listView.setVisibility(View.VISIBLE);
            itemAdapter.notifyDataSetChanged();
            itemAdapter.setNotifyOnChange(true);

            listView.invalidate();
        }
        //_________________________________________________
    }
    //_________________________________________________________________________

    /***********************************************************************
     *
     *  --------------------------  POST   REQUEST -------------------------
     *
     **********************************************************************/
    class SendLoginData extends AsyncTask<Void, Void, Void> {

        String resultString = null;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        //_________________________________________________

        @Override
        protected Void doInBackground(Void... params)
        {
            try
            {
                //String myURL = "http://site.ru/";
                String parammetrs = "param1=1&param2=XXX";
                byte[] data = null;
                InputStream is = null;

                try
                {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    conn.setRequestProperty("Content-Length", "" + Integer.toString(parammetrs.getBytes().length));
                    OutputStream os = conn.getOutputStream();
                    data = parammetrs.getBytes("UTF-8");
                    os.write(data);
                    data = null;

                    conn.connect();
                    int responseCode= conn.getResponseCode();

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    if (responseCode == 200)
                    {
                        is = conn.getInputStream();

                        byte[] buffer = new byte[8192]; // Такого вот размера буфер
                        // Далее, например, вот так читаем ответ
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1)
                        {
                            baos.write(buffer, 0, bytesRead);
                        }
                        data = baos.toByteArray();
                        resultString = new String(data, "UTF-8");
                    }
                    else
                    {
                    }
                }
                catch (MalformedURLException e)
                {
                    //resultString = "MalformedURLException:" + e.getMessage();
                }
                catch (IOException e)
                {
                    //resultString = "IOException:" + e.getMessage();
                }
                catch (Exception e)
                {
                    //resultString = "Exception:" + e.getMessage();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        //_________________________________________________

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);
            if(resultString != null)
            {
                Toast toast = Toast.makeText(getActivity().getApplicationContext(), resultString, Toast.LENGTH_SHORT);
                toast.show();
            }
        }
        //_________________________________________________
    }
    //_________________________________________________________________________
}
