package com.searchnews.rssnewsshow.rss_storage;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.searchnews.rssnewsshow.rss_storage.fragments.FragmentEchoMSK;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    PropertiesApp properties_app;

    ListView lv_all_rss;
    String[] names_rss;
    String[] url_rss_address;
    ArrayList<String> list;
    StableArrayAdapter adapter;

    public final static int CONSTANT_NUMBER_MODE_LIST_VIEW_ALL_RSS = 0;
    public final static int CONSTANT_NUMBER_MODE_LIST_DELETE_RSS = 1;

    public final static boolean PARAMETER_DELETE_ROW_RSS_TRUE = true;
    public final static boolean PARAMETER_DELETE_ROW_RSS_FALSE = false;
    public boolean parameter_delete_row = PARAMETER_DELETE_ROW_RSS_FALSE;


    int mode_work = CONSTANT_NUMBER_MODE_LIST_VIEW_ALL_RSS;

    FragmentTransaction fechomsk = null;

    public AlertDialog.Builder      alert_dial_rss;
    public int                      alert_dial_rss_delete_position;
    public String                   alert_dial_rss_delete_item;
    public View                     alert_dial_rss_delete_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        properties_app = new PropertiesApp(this);

        lv_all_rss = (ListView) findViewById(R.id.list_view_all_rss);
        names_rss = new String[]
        {
                "unian",
                "echo_moskow",
                "radio svoboda",
                "cnn",
                "fox",
                "bbc",
                "reuters",
                "sky news",
                "United Press International"        };
        url_rss_address = new String[]
        {
                "http://rss.unian.net/site/news_rus.rss",
                "http://echo.msk.ru/guests/9032/rss-fulltext.xml",
                "https://www.svoboda.org/podcast/?count=20&zoneld=17",
                "http://rss.cnn.com/rss/edition.rss",
                "http://feeds.foxnews.com/foxnews/latest",
                "http://feeds.bbci.co.uk/news/world/rss.xml",
                "http://feeds.reuters.com/reuters/topNews",
                "http://feeds.skynews.com/feeds/rss/home.xml",
                "http://rss.upi.com/news/news.rss"
        };

        createSQLiteRSS();

        list = new ArrayList<String>();

        loadListNameRSS();

        adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        lv_all_rss.setAdapter(adapter);
        lv_all_rss.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                        final String item = (String) parent.getItemAtPosition(position);
                        view.animate().setDuration(100).alpha(1)
                                .withEndAction(
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                                switch (mode_work) {
                                                    case CONSTANT_NUMBER_MODE_LIST_VIEW_ALL_RSS:

                                                        Intent intent_show_rss = new Intent(MainActivity.this, RssViewSelectedInfoActivity.class);

                                                        intent_show_rss.putExtra("adress_url", getAddressUrlPosition(position));
                                                        intent_show_rss.putExtra("name_rss", getNameRssPosition(position));
                                                        startActivity(intent_show_rss);

                                                        break;

                                                    case CONSTANT_NUMBER_MODE_LIST_DELETE_RSS:

                                                        alert_dial_rss_delete_position  = position;
                                                        alert_dial_rss_delete_item      = item;
                                                        alert_dial_rss_delete_view      = view;
                                                        alert_dial_rss.setTitle(getResources().getString(R.string.title_alert_dial)
                                                                + String.format("      %S", getNameRssPosition(position)));  // заголовок

                                                        alert_dial_rss.show();

                                                        break;
                                                }
                                            }
                                            //_______________________________________________

                                            public String getAddressUrlPosition(int position)
                                            {
                                                DataBaseHandler  db_rss = new DataBaseHandler(getApplicationContext());
                                                List<UrlRss> addressList = db_rss.getAllAddressURL();

                                                db_rss.close();
                                                return addressList.get(position).getAddressUrl();
                                            }
                                            //_______________________________________________

                                            public String getNameRssPosition(int position)
                                            {
                                                DataBaseHandler  db_rss = new DataBaseHandler(getApplicationContext());
                                                List<UrlRss> addressList = db_rss.getAllAddressURL();

                                                db_rss.close();
                                                return addressList.get(position).getName();
                                            }
                                            //_______________________________________________

                                            public void deleteObjectRssFromDB(int position)
                                            {
                                                DataBaseHandler  db_rss = new DataBaseHandler(getApplicationContext());
                                                db_rss.deleteObjectRSS(position);
                                                db_rss.close();
                                            }
                                            //_______________________________________________
                                        }
                                )
                        ;
                    }
                }
        );

        create_alert_dialog();

        setTitle(getResources().getString(R.string.app_name) + "    "
                + getResources().getString(R.string.title_activity_main_rss_show));
    }
    //_________________________________________________________________________

    @Override
    protected void onStart()
    {
        super.onStart();
    }
    //_________________________________________________________________________

    @Override
    protected void onRestart()
    {
        super.onRestart();

        loadListNameRSS();

        adapter.notifyDataSetChanged();
        adapter.setNotifyOnChange(true);
    }
    //_________________________________________________________________________

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    //_________________________________________________________________________

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    //_________________________________________________________________________

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            this.finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //_________________________________________________________________________

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_search_rss)
        {
            Intent intentFindRSS = new Intent(this, FindRSSActivity.class);

            startActivity(intentFindRSS);
        }
        else if (id == R.id.nav_show_list_rss)
        {
            mode_work = CONSTANT_NUMBER_MODE_LIST_VIEW_ALL_RSS;

            setTitle(getResources().getString(R.string.app_name) + "    "
                    + getResources().getString(R.string.title_activity_main_rss_show));
        }
        else if (id == R.id.nav_delete_rss)
        {
            mode_work = CONSTANT_NUMBER_MODE_LIST_DELETE_RSS;

            setTitle(getResources().getString(R.string.app_name) + "    "
                    + getResources().getString(R.string.title_activity_main_rss_delete));
        }

//        else if (id == R.id.nav_manage)
//        {
//
//        }
//        else if (id == R.id.nav_share)
//        {
//
//        }
//        else if (id == R.id.nav_send)
//        {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    //_________________________________________________________________________

    /************************************************************************
     *
     *                  DB SQLite - methods work with row RSS
     *
     ***********************************************************************/
    private void createSQLiteRSS()
    {
        if (properties_app.isAllTablesOfDBCreated() == properties_app.FLAG_IS_CREATE_TABLES_FALSE)
        {
            DataBaseHandler db = new DataBaseHandler(this);

            if (properties_app.getFromPreferencesCountOfRowsTable(properties_app.NUMBERNAME_TABLE_RSS) == properties_app.FLAG_IS_CREATE_TABLES_FALSE)
            {
                loadListRSSIntoDB(db);
            }
        }
    }
    //_________________________________________________________________________

    private void loadListRSSIntoDB(DataBaseHandler db_rss) {
        int i;
        //names_rss = new String[] {"unian", "echo_moskow","radio svoboda"};
        //url_rss_address = new String[] {"http://rss.unian.net/site/news_rus.rss",

        UrlRss url_rss = new UrlRss();
        int counter_name_rss = names_rss.length;
        int counter_address_rss = url_rss_address.length;

        for (i = 0; i < counter_name_rss; i++) {
            if (i < counter_name_rss && i < counter_address_rss) {
                url_rss.setName(names_rss[i]);
                url_rss.setAddressUrl(url_rss_address[i]);

                db_rss.addUrlRSS(url_rss);
            }
        }

        if (i == db_rss.getAddressURLCount())
        {
            properties_app.saveInPreferencesFlagCreatedCountOfRowsTable(properties_app.NUMBERNAME_TABLE_RSS);
        }
    }
    //_________________________________________________________________________

    private void loadListNameRSS()
    {
        list.clear();

        if (properties_app.isAllTablesOfDBCreated())
        {
            DataBaseHandler  db_rss = new DataBaseHandler(this);
            List<UrlRss> addressList = db_rss.getAllAddressURL();

            //list.clear();

            for (int i = 0; i < db_rss.getAddressURLCount(); ++i)
            {
                list.add(addressList.get(i).getName());
            }

            db_rss.close();
        }
        else
        {
            for (int i = 0; i < names_rss.length; ++i)
            {
                list.add(names_rss[i]);
            }
        }
    }
    //_________________________________________________________________________

    /**************************************************************************
     * Alert dialog - delete row RSS from data base
     * package ru.alexanderklimov.alertdialogdemo;
     */
    public void create_alert_dialog()
    {
        alert_dial_rss = new AlertDialog.Builder(MainActivity.this);

        alert_dial_rss.setTitle(getResources().getString(R.string.title_alert_dial));  // заголовок
        alert_dial_rss.setMessage(getResources().getString(R.string.message_alert_dial)); // сообщение
        alert_dial_rss.setPositiveButton(getResources().getString(R.string.btn_yes_alert_dial),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int arg1)
                    {
                        DataBaseHandler  db_rss = new DataBaseHandler(getApplicationContext());
                        db_rss.deleteObjectRSS(alert_dial_rss_delete_position);
                        db_rss.close();

                        list.remove(alert_dial_rss_delete_item);
                        adapter.notifyDataSetChanged();

                        alert_dial_rss_delete_view.setAlpha(1);
                    }
                }
                                        );

        alert_dial_rss.setNegativeButton( getResources().getString(R.string.btn_no_alert_dial),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int arg1)
                    {
                        parameter_delete_row = PARAMETER_DELETE_ROW_RSS_FALSE;
                    }
                }
                                        );

        alert_dial_rss.setCancelable(true);
        alert_dial_rss.setOnCancelListener(new DialogInterface.OnCancelListener()
                                           {
                                               public void onCancel(DialogInterface dialog)
                                               {
                                                   ;
                                               }
                                           }
        );
    }
    //_________________________________________________________________________
}
