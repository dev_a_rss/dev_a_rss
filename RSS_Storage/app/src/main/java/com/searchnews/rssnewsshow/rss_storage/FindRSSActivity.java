package com.searchnews.rssnewsshow.rss_storage;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageButton;

public class FindRSSActivity extends AppCompatActivity
{
 //   SearchView  searchView;
    WebView     webView;
    Button      btnLoadURL;
    Button      btnSaveRSS; //id_btn_add_url_rss
    EditText    editTextURL;

    ImageButton imageButton_GoBackLeft;
    ImageButton imageButton_GoForvardRight;
    ImageButton imageButton_RepeatStop;

    CheckBox    checkBox_JavaScript;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_rss);

//        searchView  = (SearchView) findViewById(R.id.id_search_view) ;
        webView     = (WebView) findViewById(R.id.id_web_view);
        //webView.getSettings().setJavaScriptEnabled(true);

        btnLoadURL  = (Button) findViewById(R.id.id_btn_load_url);
        btnSaveRSS = (Button) findViewById(R.id.id_btn_add_url_rss);
        editTextURL = (EditText) findViewById(R.id.id_editTextURL);

        checkBox_JavaScript = (CheckBox) findViewById(R.id.id_checkBox_JavaScript);

        webView.setWebViewClient(new MyWebViewClient());

        imageButton_GoBackLeft = (ImageButton) findViewById(R.id.id_imageButton_left);
        imageButton_GoForvardRight = (ImageButton) findViewById(R.id.id_imageButton_right);
        imageButton_RepeatStop = (ImageButton) findViewById(R.id.id_imageButton_stop);


    }
    //_________________________________________________________________________

//    public void OnClick_SearchView(View v)
//    {
//     webView.loadUrl(searchView.toString());
//    }
//    //_________________________________________________________________________

    private class MyWebViewClient extends WebViewClient
    {
        @Override
//        //public boolean shouldOverrideUrlLoading(WebView view, String url)
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest res)
        {
            view.loadUrl(res.toString());
            return true;
        }

//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url)
//        {
//            view.loadUrl(editTextURL.getText().toString());
//            //return true;
//            return false;
//        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            //if(Uri.parse(url).getHost().length() == 0)
            {
                return false;
            }

           // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
           // view.getContext().startActivity(intent);
           // return true;
        }
    }
    //_________________________________________________________________________

    public void OnClick_LoadURL(View v)
    {
        webView.loadUrl(editTextURL.getText().toString());
    }
    //_________________________________________________________________________

    @Override
    public void onBackPressed()
    {
        if(webView.canGoBack())
        {
            webView.goBack();
        }
        else
        {
            super.onBackPressed();
        }
    }
    //_________________________________________________________________________

    public void onClickImageButton_LeftBack(View v)
    {
        onBackPressed();
    }
    //_________________________________________________________________________

    public void onClickImageButton_RightForvard(View v)
    {
        if (webView.canGoForward())
        {
            webView.goForward();
        }
    }
    //_________________________________________________________________________

    public void onClickImageButton_StopRepeat(View v)
    {
        //if (webView.)

        webView.stopLoading();
        webView.reload();
        //if (webView.canR)
        //{
        //    webView.goForward();
        //}
    }
    //_________________________________________________________________________

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.webView.canGoBack())
        {
            this.webView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
    //_________________________________________________________________________

    public void OnClick_CheckBoxJavaScript(View v)
    {
        if (checkBox_JavaScript.isChecked()) {
            webView.getSettings().setJavaScriptEnabled(true);
        } else {
            webView.getSettings().setJavaScriptEnabled(false);
        }

        webView.reload();
    }
    //_________________________________________________________________________

    public void OnClick_AddUrlRss(View v)
    {
        Intent  intent_activity_create_params_rss = new Intent(FindRSSActivity.this, CreateParametersRssForSaveInDBActivity.class);

        intent_activity_create_params_rss.putExtra("adress_url", editTextURL.getText().toString());

        startActivity(intent_activity_create_params_rss);
    }
    //_________________________________________________________________________
}
