package com.searchnews.rssnewsshow.rss_storage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CreateParametersRssForSaveInDBActivity extends AppCompatActivity
{
    Intent  intent_get;
    String  urlString;

    EditText    ed_name;    //  editText_name_rss
    EditText    ed_address; //  editText_address_rss

    Button      btn_save;   //  btn_save_and_add_to_bdrss

    DataBaseHandler db;// = new DataBaseHandler(this);
    UrlRss          object_rss;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_parameters_rss_for_save_in_db);

        intent_get = getIntent();
        urlString = intent_get.getStringExtra("adress_url");

        setTitle(getResources().getString(R.string.title_activity_create_parameters_rss));

        ed_name =  (EditText) findViewById(R.id.editText_name_rss);
        ed_address =  (EditText) findViewById(R.id.editText_address_rss);
        ed_address.setText(urlString.toString());

        btn_save = (Button) findViewById(R.id.btn_save_and_add_to_bdrss);

        db = new DataBaseHandler(this);

        object_rss = new UrlRss();
    }
    //_________________________________________________________________________

    public void OnClick_BtnAddAndSave(View v)
    {
        object_rss.setName(ed_name.getText().toString());
        object_rss.setAddressUrl(ed_address.getText().toString());

        db.addUrlRSS(object_rss);

        finish();
    }
    //_________________________________________________________________________
}
