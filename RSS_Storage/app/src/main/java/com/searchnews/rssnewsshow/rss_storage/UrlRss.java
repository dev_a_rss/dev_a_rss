package com.searchnews.rssnewsshow.rss_storage;

/**
 * Created by sdragoev on 15.08.2017.
 */

public class UrlRss
{
    int     id;
    String  name;
    String  address_url;

    public UrlRss()
    {

    }
    //_________________________________________________________________________

    public UrlRss(int id, String name, String address_url)
    {
        this.id             = id;
        this.name           = name;
        this.address_url    = address_url;
    }
    //_________________________________________________________________________

    public UrlRss(String name, String address_url)
    {
        this.name           = name;
        this.address_url    = address_url;
    }
    //_________________________________________________________________________

    public int getID()
    {
        return id;
    }
    //_________________________________________________________________________

    public void setID(int id)
    {
        this.id = id;
    }
    //_________________________________________________________________________

    public String getName()
    {
        return name;
    }
    //_________________________________________________________________________

    public void setName(String name_rss)
    {
        name = name_rss;
    }
    //_________________________________________________________________________

    public String getAddressUrl()
    {
        return address_url;
    }
    //_________________________________________________________________________

    public void setAddressUrl(String address_url)
    {
        this.address_url = address_url;
    }
    //_________________________________________________________________________
}
