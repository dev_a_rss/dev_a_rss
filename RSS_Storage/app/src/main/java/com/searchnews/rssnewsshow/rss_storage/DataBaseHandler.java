package com.searchnews.rssnewsshow.rss_storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sdragoev on 15.08.2017.
 */

public class DataBaseHandler extends SQLiteOpenHelper
{
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "addressUrlRSS";

    // RSS table name
    private static final String TABLE_URL = "url_rss";

    // URL_RSS Table Columns names
    private static final String KEY_ID      = "id";
    private static final String KEY_NAME    = "name";
    private static final String KEY_URL_RSS = "adress_url_rss";

    String selectQuery = "SELECT  * FROM " + TABLE_URL;

    private UrlRss  url_rss;

    public DataBaseHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        url_rss = new UrlRss();
    }
    //_________________________________________________________________________

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_ADDRESS_URL_TABLE = "CREATE TABLE " + TABLE_URL
                + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                      + KEY_URL_RSS + " TEXT"
                + ")";

        db.execSQL(CREATE_ADDRESS_URL_TABLE);
    }
    //_________________________________________________________________________

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_URL);

        // Create tables again
        onCreate(db);
    }
    //_________________________________________________________________________

    // Adding new adress url
    public void addUrlRSS(UrlRss adress_url)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, adress_url.getName()); // URL Name
        values.put(KEY_URL_RSS, adress_url.getAddressUrl()); // Address URL RSS

        // Inserting Row
        db.insert(TABLE_URL, null, values);
        db.close(); // Closing database connection
    }
    //_________________________________________________________________________

    public UrlRss getUrlRSS(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_URL,
                                 new String[]
                                 {
                                        KEY_ID,
                                        KEY_NAME,
                                        KEY_URL_RSS
                                 },
                                  KEY_ID + "=?",
                                  new String[]
                                  {
                                        String.valueOf(id)
                                  },
                                  null, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
        }

        UrlRss url_address = new UrlRss(
                                    Integer.parseInt(cursor.getString(0)),
                                    cursor.getString(1),
                                    cursor.getString(2)
                                         );
        cursor.close();

        db.close();

        // return contact
        return url_address;
    }
    //_________________________________________________________________________

    public List<UrlRss> getAllAddressURL()
    {
        List<UrlRss> addressList = new ArrayList<UrlRss>();
        // Select All Query
        //String selectQuery = "SELECT  * FROM " + TABLE_URL;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst())
        {
            do
            {
                UrlRss url_rss = new UrlRss();
                url_rss.setID(Integer.parseInt(cursor.getString(0)));
                url_rss.setName(cursor.getString(1));
                url_rss.setAddressUrl(cursor.getString(2));
                // Adding contact to list
                addressList.add(url_rss);
            }
            while (cursor.moveToNext());
        }

        db.close();

        // return contact list
        return addressList;
    }
    //_________________________________________________________________________

    public int getAddressURLCount()
    {
        int     count_of_rss = 0;

        String countQuery = "SELECT  * FROM " + TABLE_URL;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        count_of_rss = cursor.getCount();
        cursor.close();

        db.close();

        return count_of_rss;
    }
    //_________________________________________________________________________

    //public int updateUrlRSS(UrlRss url_rss)
    public void updateUrlRSS(UrlRss url_rss)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, url_rss.getName());
        values.put(KEY_URL_RSS, url_rss.getAddressUrl());

        // updating row
        //return db.update(TABLE_URL, values, KEY_ID + " = ?",
        db.update(TABLE_URL, values, KEY_ID + " = ?",
                new String[] { String.valueOf(url_rss.getID()) });

        db.close();
    }
    //_________________________________________________________________________

    public void deleteUrlRSS(UrlRss url_rss)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(  TABLE_URL,
                    KEY_ID + " = ?",
                    new String[]
                            {
                                    String.valueOf(url_rss.getID())
                            }
                 );

        db.close();
    }
    //_________________________________________________________________________

    public void deleteObjectRSS(int item)
    {
//        db.delete(  TABLE_URL,
//                    KEY_ID + " = ?",
//                    new String[]
//                            {
//                                    String.valueOf(url_rss.getID())
//                            }
//                 );
//
//        db.close();
        List<UrlRss> addressList = new ArrayList<UrlRss>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst())
        {
            do
            {
                UrlRss url_rss = new UrlRss();
                url_rss.setID(Integer.parseInt(cursor.getString(0)));
                url_rss.setName(cursor.getString(1));
                url_rss.setAddressUrl(cursor.getString(2));
                // Adding contact to list
                addressList.add(url_rss);
            }
            while (cursor.moveToNext());

            deleteUrlRSS(addressList.get(item));
        }

        db.close();
    }
    //_________________________________________________________________________
}
