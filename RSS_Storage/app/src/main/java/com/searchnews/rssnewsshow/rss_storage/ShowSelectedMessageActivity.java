package com.searchnews.rssnewsshow.rss_storage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class ShowSelectedMessageActivity extends AppCompatActivity
{
    WebView     webviewShowSelectedMessage;
    String	    name_site;
    Intent      intent_get;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_selected_message);

        intent_get = getIntent();
        name_site = intent_get.getStringExtra("adress_site");

        webviewShowSelectedMessage = (WebView) findViewById(R.id.wb_show_information);
        webviewShowSelectedMessage.loadUrl(name_site.toString());
    }
    //_________________________________________________________________________

}
