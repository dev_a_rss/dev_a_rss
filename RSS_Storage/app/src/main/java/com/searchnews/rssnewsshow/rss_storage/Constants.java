package com.searchnews.rssnewsshow.rss_storage;

public interface Constants 
{
	public final String   name_file_pref = "parameters_app.txt";
	
//	public static final int  ORIGINAL_ITEM_SM_12 = 1;
//	public static final int  ORIGINAL_ITEM_SM_16 = 2;
//	public static final int  ORIGINAL_ITEM_SM_20 = 3;
//	public static final int  ORIGINAL_ITEM_SM_24 = 4;
//	public static final int  ORIGINAL_ITEM_SM_30 = 5;
//	public static final int  ORIGINAL_ITEM_SM_36 = 6;
//	public static final int  ORIGINAL_ITEM_SM_42 = 7;
//	public static final int  ORIGINAL_ITEM_SM_48 = 8;
//	public static final int  ORIGINAL_ITEM_SM_56 = 9;
//	public static final int  ORIGINAL_ITEM_SM_64 = 10;

//	public static final int  LOW_PROFILE_ITEM_SM_16 = 1;
//	public static final int  LOW_PROFILE_ITEM_SM_20 = 2;
//	public static final int  LOW_PROFILE_ITEM_SM_24 = 3;
//	public static final int  LOW_PROFILE_ITEM_SM_30 = 4;
//	public static final int  LOW_PROFILE_ITEM_SM_36 = 5;
//	public static final int  LOW_PROFILE_ITEM_SM_42 = 6;
	
//	public static final int  IMAGE_TYPE_ORIGINAL = 1;
//	public static final int  IMAGE_TYPE_LOW_PROFILE = 2;
	
//	// For reading information from web site SKF ----------------
//	public static final String	STR_NAME_PARAMETER_FOR_SITE = "number_site";
//	public static final String	STR_NAME_INFORMATION_FROM_WEBSITE = "name_value";
//
//	// From MainActivityApp -------------------------------------
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_MA = 0;

	// From DownloadsActivity -------------------------------------
	/*******************************/
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_1 = 1;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_2 = 2;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_3 = 3;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_4 = 4;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_5 = 5;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_6 = 6;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_7 = 7;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_8 = 8;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_9 = 9;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_10 = 10;
//
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_11 = 11;
	/*****************************/
	
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_12 = 12;
//	public static final int  	NUMBER_INFORMATION_FROM_WEBSITE_13 = 13;
	
	//	public static final double 	M_PI   3.14159265358979323846264338327950288;
	
//	public static  final int  INDEX_MACHINE_NOT_SELECTED	= 0;		// INDEX_SELECTED_MACHIN_0;
//	public static  final int  INDEX_MACHINE_ALTERNATOR		= 1;    	// INDEX_SELECTED_MACHIN_1
//	public static  final int  INDEX_MACHINE_COMPRESSOR		= 2;		// INDEX_SELECTED_MACHIN_2
//	public static  final int  INDEX_MACHINE_ELECTROMOTOR	= 3;		// INDEX_SELECTED_MACHIN_3
//	public static  final int  INDEX_MACHINE_GEARBOX 		= 4;		// INDEX_SELECTED_MACHIN_4
//	public static  final int  INDEX_MACHINE_GENERATOR 				= 5;	// INDEX_SELECTED_MACHIN_5
//	public static  final int  INDEX_MACHINE_PISTON_ENGINE 			= 6;	// INDEX_SELECTED_MACHIN_6
//	public static  final int  INDEX_MACHINE_PUMP 					= 7;	// INDEX_SELECTED_MACHIN_7
//	public static  final int  INDEX_MACHINE_SHAFT_SUPPORT_BEARING	= 8;	// INDEX_SELECTED_MACHIN_8
//	public static  final int  INDEX_MACHINE_THRUST_BEARING 	= 9;			// INDEX_SELECTED_MACHIN_9
//	public static  final int  INDEX_MACHINE_OTHER			= 10;			// INDEX_SELECTED_MACHIN_10
//
//	public static  final String[] data_value_request =
//		{"4", "6", "8", "10",
//		"20", "30", "40", "50", "60", "70", "80", "90", "100" };

//	//Example   https://skf-solution-factory-marine-services-674702.c.cdn77.org/downloads/SM12_Vibracon_assembly.dwg
//	//  https://skf-solution-factory-marine-services-674702.c.cdn77.org/downloads/SM16_Vibracon_LP_assembly.dwg
//	public final String  kVibraconDrawingUrl1    = "https://skf-solution-factory-marine-services-674702.c.cdn77.org/downloads/";
//	public final String  kVibraconDrawingUrl2    = "_Vibracon_assembly.dwg";
//	public final String  kVibraconLPDrawingUrl2  = "_Vibracon_LP_assembly.dwg";

//	//Example   https://skf-solution-factory-marine-services-674702.c.cdn77.org/downloads/SM12.igs
//	//  https://skf-solution-factory-marine-services-674702.c.cdn77.org/downloads/SM16LP.igs
//	public final String  kVibraconSolidUrl1     = "https://skf-solution-factory-marine-services-674702.c.cdn77.org/downloads/";
//	public final String  kVibraconSolidUrl2     = ".igs";
//
//	public static final String	SOURCE_CHOCK_ADVICE = "source_chock_advice";
}
