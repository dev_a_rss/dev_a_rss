/**
 * Created by sdragoev on 20.01.2017.
 */
package com.searchnews.rssnewsshow.rss_storage;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;


public class PostItemAdapter extends ArrayAdapter<PostData>
{
    private Activity    myContext;
    public  PostData[]  datas;

    private int         max_visible_element = 0;

    public void set_max_visible_element(int value)
    {
        max_visible_element = value;
    }
    //_________________________________________________________________________

    public PostItemAdapter(Context context, int textViewResourceId, PostData[] objects)
    {
        super(context, textViewResourceId, objects);

        // TODO Auto-generated constructor stub
        myContext   = (Activity) context;
        datas       = objects;
    }
    //_________________________________________________________________________

    public void OnItemClickListener()
    {

    }
    //_________________________________________________________________________

    public static class ViewHolder
    {
        public TextView     postTitleView;
        public TextView     postDateView;
        public TextView     postDescriptionView;
        public TextView     postLinkView;
        public ImageView    postThumbView;
    }
    //_________________________________________________________________________

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        Uri uri = null;
        URL url = null;

        if (position < max_visible_element )
        {
            if (convertView == null)
            {
                LayoutInflater inflater = myContext.getLayoutInflater();
                convertView = inflater.inflate(R.layout.postitem, null);
                //convertView = inflater.inflate(R.layout.postitem_big_image, null);

                viewHolder                  = new ViewHolder();

                viewHolder.postThumbView        = (ImageView) convertView.findViewById(R.id.postThumb);
                viewHolder.postTitleView        = (TextView) convertView.findViewById(R.id.postTitleLabel);
                viewHolder.postDescriptionView  = (TextView) convertView.findViewById(R.id.postDescriptionLabel);
                viewHolder.postDateView         = (TextView) convertView.findViewById(R.id.postDateLabel);
                viewHolder.postLinkView         = (TextView) convertView.findViewById(R.id.postLinkLabel);

                convertView.setTag(viewHolder);
            }
            else
            {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if (datas[position].postThumbUrl == null)
            {
                //viewHolder.postThumbView.setImageResource(R.drawable.postthumb_loading);
                viewHolder.postThumbView.setImageResource(R.drawable.index_image);
            }
            else
            {
                try
                {
                    url = new URL(datas[position].postThumbUrl.toString());
                    uri = Uri.parse(url.toString());
                    viewHolder.postThumbView.setImageURI(uri);
                }
                catch (java.net.MalformedURLException murl_se)
                {
                    //viewHolder.postThumbView.setImageResource(R.drawable.postthumb_loading);
                    viewHolder.postThumbView.setImageResource(R.drawable.index_image);
                }

//                if (url != null)
//                {
//                    uri = Uri.parse(url.toString());
//
//                    viewHolder.postThumbView.setImageURI(uri);
//                }
//                else
//                {
//                    viewHolder.postThumbView.setImageResource(R.drawable.postthumb_loading);
//                }
            }

            viewHolder.postTitleView.setText(datas[position].postTitle);
            viewHolder.postDescriptionView.setText(datas[position].postDescription);
            viewHolder.postDateView.setText(datas[position].postDate);
            viewHolder.postLinkView.setText(datas[position].postLink);

            convertView.setVisibility(View.VISIBLE);
        }
        else
        {
            //convertView.setVisibility(View.INVISIBLE);
            convertView.setVisibility(View.GONE);
        }

        return convertView;
    }
    //_________________________________________________________________________

    public String getPostLink(int position)
    {
        return datas[position].postLink.toString();
    }
    //_________________________________________________________________________
}
