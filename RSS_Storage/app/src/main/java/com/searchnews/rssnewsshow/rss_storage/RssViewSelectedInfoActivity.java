package com.searchnews.rssnewsshow.rss_storage;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

//import com.searchnews.rssshownews.rss_shownews.PostData;
//import com.searchnews.rssshownews.rss_shownews.PostItemAdapter;
//import com.searchnews.rssshownews.rss_shownews.R;
//import com.searchnews.rssshownews.rss_shownews.ShowSelectedMessageActivity;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.lang.Thread.sleep;

import android.support.v7.app.AppCompatActivity;

public class RssViewSelectedInfoActivity extends AppCompatActivity
{
    final int MAX_SIZE_POST_DATA = 100;

    //----------    type_array_sorting  ------------
    final int TYPE_ARRAY_SORTING_TITLE              = 0;
    final int TYPE_ARRAY_SORTING_TITLE_BACK         = 1;
    final int TYPE_ARRAY_SORTING_DESCRIPTION        = 2;
    final int TYPE_ARRAY_SORTING_DESCRIPTION_BACK   = 3;
    final int TYPE_ARRAY_SORTING_DATE_TIME          = 4;
    final int TYPE_ARRAY_SORTING_DATE_TIME_BACK     = 5;
    private int type_array_sorting = TYPE_ARRAY_SORTING_TITLE;

    private PostData[]  listData;
    private PostData[]  listDataShow;
    private PostData[]  listData_new;
    ListView            listView;
    int     number_elt = 0;

    private PostData[]  listDataSort;

    Context     f_context;
    PostItemAdapter itemAdapter;

    int                 receive_bytes;

    /**************************** New code ******************************/
    private String title        = "title";
    private String link         = "link";
    private String description  = "description";
    private String pubDate      = "pubDate";
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean     parsingComplete = true;
    /**************************** New code ******************************/

    HTTPDownloadTask    rss_packet;
    //String              urlString = new String("http://echo.msk.ru/guests/9032/rss-fulltext.xml");
    String              urlString;// = new String("http://rss.unian.net/site/news_rus.rss");
    String              nameRssStringForTitle;// = new String("http://rss.unian.net/site/news_rus.rss");

    Intent  intent_get;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_view_selected_info);

        //intent_show_rss.putExtra("adress_url", url_rss_address[position]);
        intent_get = getIntent();
        urlString = intent_get.getStringExtra("adress_url");
        nameRssStringForTitle = intent_get.getStringExtra("name_rss");

        setTitle(getResources().getString(R.string.app_name) + String.format("      %S", nameRssStringForTitle));

        listView = (ListView) findViewById(R.id.postListViewF);
        listView.setVisibility(View.INVISIBLE);

        generateDummyData();

        rss_packet = new HTTPDownloadTask();
        rss_packet.execute(urlString);
    }
    //_________________________________________________________________________

    @Override
    protected void onStart()
    {
        super.onStart();
    }
    //_________________________________________________________________________

    /**************************************************************************
     *
     *                          MENU
     *
     **************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_rss_view_selected_info , menu);
        return true;
    }
    //_________________________________________________________________________

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_sort_title_a_z:
                    type_array_sorting = TYPE_ARRAY_SORTING_TITLE;
                    createArrayForSorting(type_array_sorting);
                return true;

            case R.id.action_sort_title_z_a:
                    type_array_sorting = TYPE_ARRAY_SORTING_TITLE_BACK;
                    createArrayForSorting(type_array_sorting);
                return true;

            case R.id.action_sort_description_a_z:
                    type_array_sorting = TYPE_ARRAY_SORTING_DESCRIPTION;
                    createArrayForSorting(type_array_sorting);
                return true;

            case R.id.action_sort_description_z_a:
                    type_array_sorting = TYPE_ARRAY_SORTING_DESCRIPTION_BACK;
                    createArrayForSorting(type_array_sorting);
                return true;

            case R.id.action_sort_time_0_24:
                    type_array_sorting = TYPE_ARRAY_SORTING_DATE_TIME;
                    createArrayForSorting(type_array_sorting);
                return true;

            case R.id.action_sort_time_24_0:
                    type_array_sorting = TYPE_ARRAY_SORTING_DATE_TIME_BACK;
                    createArrayForSorting(type_array_sorting);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //_________________________________________________________________________

    private void createArrayForSorting(int type_array_sorting)
    {
        // 1. Create array from title of listView  ---------------
        char[]      array_symbol            = new char[number_elt];
        char[]      array_symbol_sort       = new char[number_elt];
        char[]      array_symbol_sort_back  = new char[number_elt];

        String[]    array_str_date              = new String[number_elt];
        String[]    array_str_date_sort         = new String[number_elt];
        String[]    array_str_date_sort_back    = new String[number_elt];

        if (type_array_sorting == TYPE_ARRAY_SORTING_TITLE
                ||
                type_array_sorting == TYPE_ARRAY_SORTING_TITLE_BACK
           )
        {
            for (int i = 0; i < number_elt; i++)
            {
                array_symbol_sort[i] = listData[i].postTitle.charAt(0);
                if (Character.isLetterOrDigit(array_symbol_sort[i]))
                {
                    array_symbol[i] = listData[i].postTitle.charAt(0);
                }
                else
                {
                    array_symbol_sort[i] = listData[i].postTitle.charAt(1);
                    array_symbol[i] = listData[i].postTitle.charAt(1);
                }
            }
        }
        else if (type_array_sorting == TYPE_ARRAY_SORTING_DESCRIPTION
                ||
                type_array_sorting == TYPE_ARRAY_SORTING_DESCRIPTION_BACK
                )
        {
            for (int i = 0; i < number_elt; i++)
            {
                if (listData[i].postDescription != null)
                {
                    array_symbol_sort[i] = listData[i].postDescription.charAt(0);
                    if (Character.isLetterOrDigit(array_symbol_sort[i])) {
                        array_symbol[i] = listData[i].postDescription.charAt(0);
                    } else {
                        array_symbol_sort[i] = listData[i].postDescription.charAt(1);
                        array_symbol[i] = listData[i].postDescription.charAt(1);
                    }
                }
                else
                {
                    array_symbol_sort[i] = '1';
                    array_symbol[i] = '1';
                }
            }

        }
        else if (type_array_sorting == TYPE_ARRAY_SORTING_DATE_TIME
                 ||
                 type_array_sorting == TYPE_ARRAY_SORTING_DATE_TIME_BACK
                )
        {
            for (int i = 0; i < number_elt; i++)
            {
                array_str_date[i]       = new String(listData[i].postDate);
                array_str_date_sort[i]  = new String(listData[i].postDate);
            }
        }

        // 2. Sorting this array ---------------------------------
        if (type_array_sorting >= TYPE_ARRAY_SORTING_TITLE
            && type_array_sorting <= TYPE_ARRAY_SORTING_DESCRIPTION_BACK
            )
        {
            Arrays.sort(array_symbol_sort);                            // A-Z

            if (type_array_sorting == TYPE_ARRAY_SORTING_TITLE_BACK    // Z-A
                    ||
                    type_array_sorting == TYPE_ARRAY_SORTING_DESCRIPTION_BACK
                    ) {
                for (int i = 0; i < number_elt; i++) {
                    array_symbol_sort_back[i] = array_symbol_sort[(number_elt - 1) - i];
                }
                for (int i = 0; i < number_elt; i++) {
                    array_symbol_sort[i] = array_symbol_sort_back[i];
                }
            }
        }
        else if (type_array_sorting == TYPE_ARRAY_SORTING_DATE_TIME
                ||
                type_array_sorting == TYPE_ARRAY_SORTING_DATE_TIME_BACK
                )
        {
            Arrays.sort(array_str_date_sort);                            // A-Z

            if (type_array_sorting == TYPE_ARRAY_SORTING_DATE_TIME_BACK)
            {
                for (int i = 0; i < number_elt; i++)
                {
                    array_str_date_sort_back[i] = array_str_date_sort[(number_elt - 1) - i];
                }
                for (int i = 0; i < number_elt; i++)
                {
                    array_str_date_sort[i] = array_str_date_sort_back[i];
                }
            }
        }

        // 3. Resort listView ------------------------------------
        listDataSort = null;
        listDataSort = new PostData[number_elt + 1];

        if (type_array_sorting >= TYPE_ARRAY_SORTING_TITLE
                && type_array_sorting <= TYPE_ARRAY_SORTING_DESCRIPTION_BACK
                )
        {
            for (int i = 0; i < number_elt; i++) {
                for (int j = 0; j < number_elt; j++) {
                    if (i < number_elt && array_symbol_sort[i] == array_symbol[j]) {
                        listDataSort[i] = new PostData();

                        listDataSort[i] = listDataShow[j];

                        array_symbol[j] = 0;
                        i++;
                        j = -1;
                    }
                }
            }
        }
        else if (type_array_sorting == TYPE_ARRAY_SORTING_DATE_TIME
                ||
                type_array_sorting == TYPE_ARRAY_SORTING_DATE_TIME_BACK
                )
        {
            for (int i = 0; i < number_elt; i++)
            {
                for (int j = 0; j < number_elt; j++)
                {
                    if (i < number_elt
                            &&  array_str_date[j] != null
                            &&  array_str_date_sort[i].equals(array_str_date[j]))
                    {
                        listDataSort[i] = new PostData();

                        listDataSort[i] = listDataShow[j];

                        array_str_date[j] = null;
                        i++;
                        j = -1;
                    }
                }
            }
        }

        for (int i = 0; i < number_elt; i++)
        {
            listData[i] = listDataSort[i];
        }

        for (int i = 0; i < number_elt; i++)
        {
            listDataShow[i] = listData[i];
        }

        itemAdapter = new PostItemAdapter(this, R.layout.postitem, listDataShow);
        itemAdapter.set_max_visible_element(number_elt);
        listView.setAdapter(itemAdapter);
    }

    /**************************************************************************
     *
     *                               Adapter
     *
     **************************************************************************/
    public void setAdapterAndShowItems()
    {
        createListData();

        itemAdapter = new PostItemAdapter(this, R.layout.postitem, listDataShow);
        //itemAdapter = new PostItemAdapter(this, R.layout.postitem_big_image, listDataShow);
        itemAdapter.set_max_visible_element(number_elt);
        listView.setAdapter(itemAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            private PostItemAdapter.ViewHolder viewHolder;

            @Override
            public void onItemClick(AdapterView adapterView, View itemClicked, int position, long id)
            {
                Intent  intentShowSiteWithMessage = new Intent(getApplicationContext(), ShowSelectedMessageActivity.class);

                this.viewHolder = (PostItemAdapter.ViewHolder) itemClicked.getTag();

                this.viewHolder.postLinkView.setId(position);

                String strURLMessage = new String(this.viewHolder.postLinkView.getText().toString());

                intentShowSiteWithMessage.putExtra("adress_site", strURLMessage.toString());

                startActivity(intentShowSiteWithMessage);
            }
        });

    }
    //_________________________________________________________________________

    private void generateDummyData()
    {
        PostData data = null;
        listData = new PostData[MAX_SIZE_POST_DATA];

        for (int i = 0; i < MAX_SIZE_POST_DATA; i++)
        {
            data = new PostData();

            data.postThumbUrl = null;
            data.postTitle = "Post " + (i + 1) + " Title: This is the Post Title from RSS Feed";
            data.postDescription = "This is description of item";
            data.postDate = "January 23, 2017";
            data.postLink = null;

            listData[i] = data;
        }
    }
    //_________________________________________________________________________

    public void setStringURL(String  strURL)
    {
        // Required empty public constructor
        urlString = new String(strURL.toString());
    }
    //_________________________________________________________________________

    /**************************************************************************
     *
     *                                   New code                          star
     *      * @return
     *************************************************************************/
//    public String getTitle()
//    {
//        return title;
//    }
//    //_________________________________________________________________________

    public String getLink()
    {
        return link;
    }
    //_________________________________________________________________________

    public String getDescription()
    {
        return description;
    }
    //_________________________________________________________________________

    public String getDate()
    {
        return pubDate;
    }
    //_________________________________________________________________________

    public void parseXMLAndStoreIt(XmlPullParser myParser)
    {
        int         event;
        String      text=null;
        int         counter_elements_item = 0;
        PostData    data = null;

        data = new PostData();
        number_elt = 0;
        int     x;

        //itemAdapter.clear();

        try {
            event = myParser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT)
            {
                String name = myParser.getName();

                switch (event)
                {
                    case XmlPullParser.START_TAG:
                        break;

                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        Log.d("text = ", text);

                        break;

                    case XmlPullParser.END_TAG:

                        if(name.equals("title")){
                            title = text;
                            data.postTitle = text;
                            Log.d("title = ", title);
                            counter_elements_item++;
                        }
                        else if(name.equals("link")
                                ||
                                name.equals("guid")  &&  text.contains("http")  //  for itunes rss 2.0
                               )
                        {
                            link = text;
                            data.postLink = text;
                            Log.d("link = ", text);
                            counter_elements_item++;
                        }
                        else if(name.equals("description"))
                        {
//                            description = text;
//                            data.postDescription = text;
//                            Log.d("description = ", title);
//                            counter_elements_item++;

                            description = text;

                            if (isTextContainZeroTags(text))
                            {
                                data.postDescription = text;
                                Log.d("description = ", title);
                                counter_elements_item++;
                            }
                            else
                            {
                                String text_descript = getTextDescription(text);
                                data.postDescription = text_descript;
                                Log.d("description = ", text_descript);
                                counter_elements_item++;
                            }
                        }
                        else if(name.equals("pubDate")
                                ||
                                name.equals("lastUpdated")
                            //    ||
                            //    name.equals("lastBuildDate")
                                )
                        {
                            pubDate = text;
                            data.postDate = text;
                            Log.d("Date = ", text);
                            counter_elements_item++;
                        }
                        else if (name.equals("item"))
                        {
                            listData[number_elt++] = data;

                            data = new PostData();
                        }
                        else if (name.equals("/item"))
                        {
                            x = 2;
                        }
                        else if (name.equals("enclosure")                             // for rss v.2.0
                                 &&
                                 myParser.getAttributeValue(2) != null
                                 &&
                                 isAttributeImage(myParser.getAttributeValue(2)) == true
                                )
                        {
                            //String  enclose = myParser.getInputEncoding();
                            String  name_attribute = myParser.getAttributeName(0);
                            String  image_url = myParser.getAttributeValue(0);

                            data.postThumbUrl = image_url;
                            x = 3;
                        }
                        else if (name.equals("itunes:image")                             // for rss v.2.0
                                 &&
                                 myParser.getAttributeValue(0) != null
                                 &&
                                 isAttributeImage(myParser.getAttributeValue(0)) == true
                                )
                        {
                            //String  name_attribute = myParser.getAttributeName(0);
                            String  image_url = myParser.getAttributeValue(0);

                            data.postThumbUrl = image_url;
                            x = 3;
                        }
                        else
                        {
                            x = 4;
                        }

                        break;
                }

                event = myParser.next();
            }

            parsingComplete = false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    //_________________________________________________________________________

    private boolean isAttributeImage(String value_attribute)
    {
        if (value_attribute.contains("image")
            ||
            value_attribute.contains("jpg")
           )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //_________________________________________________________________________

    private boolean isTextContainZeroTags(String search_text)
    {
        boolean ret_value = true;

        if (search_text.contains("<p>")
            ||
            search_text.contains("<a")
                    ||
            search_text.contains("<img")
           )
        {
            ret_value = false;
        }

        return ret_value;
    }
    //_________________________________________________________________________

    /**************************************************************************
    * This method mast get text from parserXML in TAG description when there
    * are many tags insert in this message
    */
    private String getTextDescription(String text_descript_Atom)
    {
        StringBuffer    strBuffDest = null;
        String          ret_str = null;
        int             size_text = text_descript_Atom.length();

        char[]          chars_text_sour = new char[size_text];
        char[]          chars_text_dest; // = new char[size_text];
        boolean         flag_start_teg = false;
        int             counter_tags = 0;
        int             strart_simbol = 0, end_simbol = 0;

        text_descript_Atom.getChars(0, size_text, chars_text_sour, 0);

        for (int i = 0; i < size_text; i++)
        {
            if (flag_start_teg == false
                &&
                chars_text_sour[i] == '<'
                &&
                chars_text_sour[i+1] == 'p'
                &&
                chars_text_sour[i+2] == '>'
               )
            {
                end_simbol = strart_simbol = i + 3;
                i += 2;
                flag_start_teg = true;
            }

            if (flag_start_teg == true
                    &&
                    chars_text_sour[i] == '<'
                    &&
                    chars_text_sour[i+1] == '/'
                    &&
                    chars_text_sour[i+2] == 'p'
                    &&
                    chars_text_sour[i+3] == '>'
                    )
            {
                end_simbol = i - 1;
                counter_tags++;
                flag_start_teg = false;
            }

            if (counter_tags == 3)
            {
                counter_tags++;
                int size_dest = end_simbol - strart_simbol + 1;
                chars_text_dest = new char[size_dest];

                for (int j = strart_simbol; j < end_simbol; j++)
                {
                    chars_text_dest[j - strart_simbol] = chars_text_sour[j];
                }

                ret_str = new String(chars_text_dest);
            }
        }

        if (counter_tags < 3)
        {
            //ret_str = new String(text_descript_Atom);
            ret_str = new String(deleteAllOtherParametersInDescription(text_descript_Atom));
        }

        return ret_str;
    }
    //_________________________________________________________________________

    private String deleteAllOtherParametersInDescription(String str_source_text)
    {
        String  ret_str = null;

        int     size_text = str_source_text.length();
        char[]  chars_source = new char[size_text];
        char[]  chars_destin = null; //new char[size_text];

        final char SIMBOL_START_TAG = '<';
        final char SIMBOL_END_TAG   = '>';

        int     simbol_start_chars = 0,
                simbol_end_chars = 0;
        int     simbol_start_del = 0,
                simbol_end_del = 0;

        boolean flag_start_delete = false;

        str_source_text.getChars(0, size_text, chars_source, 0);

        // Find text wich must be deleted -----------------
        for (int i = 0; i < size_text; i++)
        {
            if (chars_source[i] == SIMBOL_START_TAG  &&  i ==0)
            {
                simbol_start_del = 0;
                flag_start_delete = true;
            }
            else if (chars_source[i] == SIMBOL_START_TAG  &&  flag_start_delete == false)
            {
                simbol_start_del = i;
                flag_start_delete = true;
            }
            else if (chars_source[i] == SIMBOL_END_TAG)
            {
                simbol_end_del = i;
            }
        }

//        if (flag_start_delete == true)
//        {
//            int j = 0;
//            int size_new_chars = simbol_end_del - simbol_start_del + 1;
//            chars_destin = new char[size_new_chars];
//
//            for (int i = 0; i < size_text; i++)
//            {
//                if (i < simbol_start_del || i > simbol_end_del)
//                {
//                    chars_destin[j++] = chars_source[i];
//                }
//            }
//
//            ret_str = new String(chars_destin);
//        }
        if (flag_start_delete == true  &&  simbol_start_del > 0)
        {
            int j = 0;
            int size_new_chars = simbol_end_del - simbol_start_del + 1;
            chars_destin = new char[size_new_chars];

            for (int i = 0; i < size_text; i++)
            {
                if (i < simbol_start_del || i > simbol_end_del)
                {
                    chars_destin[j++] = chars_source[i];
                }
            }

            ret_str = new String(chars_destin);
        }
        else if (flag_start_delete == true  &&  simbol_start_del == 0)
        {
            int j = 0;
            int size_new_chars = size_text - simbol_end_del;

            chars_destin = new char[size_new_chars];

            for (int i = 0; i < size_text; i++)
            {
                if (i > simbol_end_del)
                {
                    chars_destin[j++] = chars_source[i];
                }
            }

            ret_str = new String(chars_destin);
        }
        else
        {
            ret_str = new String(str_source_text);
        }

        return ret_str;
    }
    //_________________________________________________________________________

    private void createListData()
    {
        listDataShow = null;
        listDataShow = new PostData[number_elt];

        for (int i = 0; i < number_elt; i++)
        {
            listDataShow[i] = listData[i];
        }
    }
    //_________________________________________________________________________

    /***********************************************************************
     *
     *  ---------------------------  GET   REQUEST -------------------------
     *
     **********************************************************************/
    private class HTTPDownloadTask extends AsyncTask< String, Integer, PostData[]>
    {

        @Override
        protected PostData[] doInBackground(String... params)
        {
            // TODO Auto-generated method stub
            String urlStr = params[0];
            InputStream is = null;
            try
            {
                URL url = new URL(urlStr);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10*1000);
                connection.setConnectTimeout(10*1000);
                connection.setRequestMethod("GET");
                connection.setDoInput(true);

                connection.setChunkedStreamingMode(32000);    // Added 2017.01.22  10:52
                connection.setIfModifiedSince(0L);          // Added 2017.01.22  10:52

                connection.setRequestProperty( "charset", "utf-8");

                connection.connect();
                int response = connection.getResponseCode();
                Log.d("debug", "The response is: " + response);
                is = connection.getInputStream();

                xmlFactoryObject = XmlPullParserFactory.newInstance();
                XmlPullParser myparser = xmlFactoryObject.newPullParser();

                myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                myparser.setInput(is, null);

                parseXMLAndStoreIt(myparser);
                is.close();

                Log.d("debug  size counter = ", String.valueOf(receive_bytes));
            }
            catch (MalformedURLException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (XmlPullParserException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values)
        {
            super.onProgressUpdate(values);
        }
        //_________________________________________________

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
        //_________________________________________________

        @Override
        protected void onPostExecute(PostData[] postDatas)
        {
            super.onPostExecute(postDatas);

            setAdapterAndShowItems();

            listView.setVisibility(View.VISIBLE);
            itemAdapter.notifyDataSetChanged();
            itemAdapter.setNotifyOnChange(true);

            listView.invalidate();
        }
        //_________________________________________________
    }
    //_________________________________________________________________________
}
