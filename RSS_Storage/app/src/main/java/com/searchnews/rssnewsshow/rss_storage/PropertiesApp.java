package com.searchnews.rssnewsshow.rss_storage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sdragoev on 18.08.2017.
 */

public class PropertiesApp implements Constants
{
    SharedPreferences sPref;
    Context cont;

    final String SAVED_INT = "saved_int";

    private final static String  key_table_rss 	= "url_rss";

    public final  int  NUMBERNAME_TABLE_RSS 			= 1;

    public final static boolean FLAG_CREATE_TABLE_FALSE = false;
    public final static boolean FLAG_CREATE_TABLE_TRUE  = true;
    private boolean 	flag_create_tab_rss 	= FLAG_CREATE_TABLE_FALSE;


    public final boolean    FLAG_IS_CREATE_TABLES_FALSE     = false;
    public final boolean    FLAG_IS_CREATE_TABLES_TRUE      = true;
    boolean 			    flagIsCreateTables 	            = FLAG_IS_CREATE_TABLES_FALSE;
    private final static String KEY_CREATE_DB 		        = "flag_create_db";


    public PropertiesApp(Context cnt)
    {
        cont = cnt;
        sPref =  cont.getSharedPreferences(name_file_pref, android.content.Context.MODE_PRIVATE);

        //loadParamSystem();

        loadFlagsAboutDBRSS();

        //loadCounterRequestVibracons();

        //loadNumberBoltHole();
        //loadHoleSize();
        //loadHoleHeight();
    }
    //_________________________________________________________________________

    public String getFileNamePreferences()
    {
        return name_file_pref;
    }
    //_________________________________________________________________________

    public boolean isCreateAllTables()
    {
        return flagIsCreateTables;
    }
    //_________________________________________________________________________

    public void loadFlagsAboutDBRSS()
    {
        flagIsCreateTables = sPref.getBoolean(KEY_CREATE_DB, FLAG_IS_CREATE_TABLES_FALSE);

        flag_create_tab_rss 	= sPref.getBoolean(key_table_rss ,		FLAG_CREATE_TABLE_FALSE);
//        flag_create_tab_bolt 		= sPref.getBoolean(key_table_bolt,			FLAG_CREATE_TABLE_FALSE);
//        flag_create_tab_vibracons 	= sPref.getBoolean(key_table_vibracons,		FLAG_CREATE_TABLE_FALSE);
//        flag_create_tab_equipment 	= sPref.getBoolean(key_table_equipment,		FLAG_CREATE_TABLE_FALSE);
//        flag_create_tab_corr_factor = sPref.getBoolean(key_table_corrfactor,	FLAG_CREATE_TABLE_FALSE);
//        flag_create_tab_mchine_type = sPref.getBoolean(key_table_machtype,		FLAG_CREATE_TABLE_FALSE);
//        flag_create_tab_value_unit	= sPref.getBoolean(key_table_value_unit, 	FLAG_CREATE_TABLE_FALSE);
//        flag_create_tab_imp			= sPref.getBoolean(key_table_imp, 			FLAG_CREATE_TABLE_FALSE);
    }
    //_________________________________________________________________________

    public boolean getFromPreferencesCountOfRowsTable(int number_table)
    {
        boolean flag_return = FLAG_CREATE_TABLE_FALSE;

        switch(number_table)
        {
            case NUMBERNAME_TABLE_RSS:  flag_return = flag_create_tab_rss; 	break;
//            case NUMBERNAME_TABLE_BOLT:      flag_return = flag_create_tab_bolt;		break;
//            case NUMBERNAME_TABLE_VIBRACONS: flag_return = flag_create_tab_vibracons;	break;
//            case NUMBERNAME_TABLE_EQUIPMENT: flag_return = flag_create_tab_equipment;	break;
//            case NUMBERNAME_TABLE_CORRECTION_FACTOR: flag_return = flag_create_tab_corr_factor; break;
//            case NUMBERNAME_TABLE_MACHINE_TYPE: flag_return = flag_create_tab_mchine_type; 		break;
//            case NUMBERNAME_TABLE_VALUE_UNITS: 	flag_return = flag_create_tab_value_unit; 		break;
//            case NUMBERNAME_TABLE_IMP: 			flag_return = flag_create_tab_imp; 				break;

            default: flag_return = FLAG_CREATE_TABLE_FALSE;
        }

        return flag_return;
    }
    //_________________________________________________________________________

    public void saveInPreferencesFlagCreatedCountOfRowsTable(int number_table)
    {
        SharedPreferences.Editor ed = sPref.edit();

        switch(number_table)
        {
            case NUMBERNAME_TABLE_RSS:	   ed.putBoolean(key_table_rss, FLAG_IS_CREATE_TABLES_TRUE); 	    	break;
//            case NUMBERNAME_TABLE_BOLT:    		ed.putBoolean(key_table_bolt, FLAG_IS_CREATE_TABLES_TRUE);	    		break;
//            case NUMBERNAME_TABLE_VIBRACONS:	ed.putBoolean(key_table_vibracons, FLAG_IS_CREATE_TABLES_TRUE);	    	break;
//            case NUMBERNAME_TABLE_EQUIPMENT:	ed.putBoolean(key_table_equipment, FLAG_IS_CREATE_TABLES_TRUE);	    	break;
//            case NUMBERNAME_TABLE_CORRECTION_FACTOR:	ed.putBoolean(key_table_corrfactor, FLAG_IS_CREATE_TABLES_TRUE);break;
//            case NUMBERNAME_TABLE_MACHINE_TYPE:    		ed.putBoolean(key_table_machtype, FLAG_IS_CREATE_TABLES_TRUE);	break;
//            case NUMBERNAME_TABLE_VALUE_UNITS:    		ed.putBoolean(key_table_value_unit, FLAG_IS_CREATE_TABLES_TRUE);break;
//            case NUMBERNAME_TABLE_IMP:    				ed.putBoolean(key_table_imp, FLAG_IS_CREATE_TABLES_TRUE);		break;
        }

        ed.commit();
    }
    //_________________________________________________________________________

    public boolean isAllTablesOfDBCreated()
    {
        if (flag_create_tab_rss == FLAG_IS_CREATE_TABLES_TRUE
//                &&
//                flag_create_tab_bolt == FLAG_IS_CREATE_TABLES_TRUE
//                &&
//                flag_create_tab_vibracons == FLAG_IS_CREATE_TABLES_TRUE
//                &&
//                flag_create_tab_equipment == FLAG_IS_CREATE_TABLES_TRUE
//                &&
//                flag_create_tab_corr_factor == FLAG_IS_CREATE_TABLES_TRUE
//                &&
//                flag_create_tab_mchine_type == FLAG_IS_CREATE_TABLES_TRUE
//                &&
//                flag_create_tab_value_unit == FLAG_IS_CREATE_TABLES_TRUE
//                &&
//                flag_create_tab_imp == FLAG_IS_CREATE_TABLES_TRUE
                )
        {
            return FLAG_IS_CREATE_TABLES_TRUE;
        }

        return FLAG_CREATE_TABLE_FALSE;
    }
    //_________________________________________________________________________


}
